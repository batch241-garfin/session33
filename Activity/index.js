
// 4. NOt finish this yet

async function fetchData()
{
	let result = await fetch('https://jsonplaceholder.typicode.com/todos/');

	let json = await result.json();

	let numberMap = json.map(function(number)
	{
		let numbers = number;
		return numbers.title
	})
	console.log(numberMap)
};

fetchData();

// 5.

fetch('https://jsonplaceholder.typicode.com/todos/199',
{
	method: 'GET',
	headers: {
		'Content-Type': 'application/json'
	}	
})
.then((response) => response.json())
.then((json) => console.log(json));

// 6. Didn't get this :(

fetch('https://jsonplaceholder.typicode.com/todos/199',
{
	method: 'GET',
	headers: {
		'Content-Type': 'application/json'
	}	
})
.then((response) => response.json())
.then((json) => console.log(`Title: ${json.title}, Status: ${json.completed}`));


// 7.

fetch('https://jsonplaceholder.typicode.com/todos/',
{
	method: 'POST',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
        "userId": 12,
        "title": "dfsdjfb fdjfbdfjbflsddf",
        "completed": true
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

// 8.

fetch('https://jsonplaceholder.typicode.com/todos/200',
{
	method: 'PUT',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		"userId": 2,
        "title": "dufhdjf djfdf",
        "description": "dfdfd dgfdg",
        "completed": false,
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

// 9.

fetch('https://jsonplaceholder.typicode.com/todos/200',
{
	method: 'PUT',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
        "title": "",
        "description": "",
        "status": "",
        "datecompleted": '',
        "userId": ''
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

// 10.

fetch('https://jsonplaceholder.typicode.com/todos/200', {
method: 'PATCH',
body: JSON.stringify({
completed: true
}),
headers: {
'Content-type': 'application/json'
}
})
.then(response => response.json())
.then(json => console.log(json))

// 11.

fetch('https://jsonplaceholder.typicode.com/todos/199',
{
	method: 'PUT',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		"userId": 12,
        "title": "aaadufhdjf djfdf",
        "description": "adfdfd dgfdg",
        "completed": true,
        "datecompleted": "01/01/22"
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

// 12.

fetch('https://jsonplaceholder.typicode.com/todos/199',
{
	method: 'DELETE',
});

// 13.

//postman

// 14.

//postman

// 15.

//postman

// 16.

//postman

// 17.

//postman

// 18.

//postman

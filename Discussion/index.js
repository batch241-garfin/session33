console.log("");

// Javascript Synchronous vs Asynchronous

// asynv=chronous means that we can procedd to execute other statments, while time consuming code is running in the background.

// fetch() method

// Returns a promise tha resolves to a response object. A promise is an object that represents the eventual completion (or failure) of an asynchronous function and its resulting value.

console.log(fetch('https://jsonplaceholder.typicode.com/posts'));

// .then method captures the response object and returns another promise which will eventually be resolved or rejected

fetch('https://jsonplaceholder.typicode.com/posts').then(response => console.log(response.status));

// json method - from the response object to convert the data retrieved into JSON format to be used in our application


fetch('https://jsonplaceholder.typicode.com/posts')
.then(response => response.json())
.then((json) => console.log(json));
// print the converted JSON value from the fetch request
// promise chain - using multiple .then()


// Async and await

async function fetchData()
{
	let result = await fetch('https://jsonplaceholder.typicode.com/posts');
	console.log(result);
	console.log(typeof result);
	console.log(result.body);

	let json = await result.json();
	console.log(json);
};

fetchData();

// Creating a post

//

fetch('https://jsonplaceholder.typicode.com/posts',
{
	// Sets the method of the request object
	method: 'POST',

	// Sets the header data of the request object to be sent to the backend
	headers: {
		'Content-Type': 'application/json'
	},

	// JSON.stringify - converts the object data into a srtringified JSON
	body: JSON.stringify({
        "userId": 1,
        "title": "Edit Post",
        "body": "Edit Post File"
	})
})
.then((response) => response.json())
.then((json) => console.log(json));


// Updating a post


fetch('https://jsonplaceholder.typicode.com/posts/100',
{
	// Sets the method of the request object
	method: 'PUT',

	// Sets the header data of the request object to be sent to the backend
	headers: {
		'Content-Type': 'application/json'
	},

	// JSON.stringify - converts the object data into a srtringified JSON
	body: JSON.stringify({
        "userId": 2,
        "title": "Update Post",
        "body": "Updated Post File"
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

// Deleting a post

fetch('https://jsonplaceholder.typicode.com/posts/100',
{
	method: 'DELETE',
});

